package com.example.simpletodoapp.model;

import java.io.Serializable;

public class SideMenuModel implements Serializable {

    int sideMenuImage;
    String menuName;

    public SideMenuModel(int sideMenuImage, String menuName) {
        this.sideMenuImage = sideMenuImage;
        this.menuName = menuName;
    }

    public int getSideMenuImage() {
        return sideMenuImage;
    }

    public void setSideMenuImage(int sideMenuImage) {
        this.sideMenuImage = sideMenuImage;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
