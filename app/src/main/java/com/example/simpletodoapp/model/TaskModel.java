package com.example.simpletodoapp.model;

import java.io.Serializable;

public class TaskModel implements Serializable {

    String mainKey;
    String title;
    String desc;
    String remindMeDateTime;
    String dueDate;

    public TaskModel() {
    }

    public TaskModel(String title, String desc, String remindMeDateTime, String dueDate) {
        this.title = title;
        this.desc = desc;
        this.remindMeDateTime = remindMeDateTime;
        this.dueDate = dueDate;
    }

    public String getMainKey() {
        return mainKey;
    }

    public void setMainKey(String mainKey) {
        this.mainKey = mainKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRemindMeDateTime() {
        return remindMeDateTime;
    }

    public void setRemindMeDateTime(String remindMeDateTime) {
        this.remindMeDateTime = remindMeDateTime;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
