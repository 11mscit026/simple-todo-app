package com.example.simpletodoapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class getTaskModel {

    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("dueDate")
    @Expose
    private String dueDate;
    @SerializedName("remindMeDateTime")
    @Expose
    private String remindMeDateTime;
    @SerializedName("t7vu_fugugug_9-6-2021")
    @Expose
    private T7vuFugugug962021 t7vuFugugug962021;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("vjvjchhvvuch24-6-2021 13:1524-6-2021")
    @Expose
    private Vjvjchhvvuch246202113152462021 vjvjchhvvuch246202113152462021;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getRemindMeDateTime() {
        return remindMeDateTime;
    }

    public void setRemindMeDateTime(String remindMeDateTime) {
        this.remindMeDateTime = remindMeDateTime;
    }

    public T7vuFugugug962021 getT7vuFugugug962021() {
        return t7vuFugugug962021;
    }

    public void setT7vuFugugug962021(T7vuFugugug962021 t7vuFugugug962021) {
        this.t7vuFugugug962021 = t7vuFugugug962021;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Vjvjchhvvuch246202113152462021 getVjvjchhvvuch246202113152462021() {
        return vjvjchhvvuch246202113152462021;
    }

    public void setVjvjchhvvuch246202113152462021(Vjvjchhvvuch246202113152462021 vjvjchhvvuch246202113152462021) {
        this.vjvjchhvvuch246202113152462021 = vjvjchhvvuch246202113152462021;
    }

    public class T7vuFugugug962021 {

        @SerializedName("desc")
        @Expose
        private String desc;
        @SerializedName("dueDate")
        @Expose
        private String dueDate;
        @SerializedName("remindMeDateTime")
        @Expose
        private String remindMeDateTime;
        @SerializedName("title")
        @Expose
        private String title;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getRemindMeDateTime() {
            return remindMeDateTime;
        }

        public void setRemindMeDateTime(String remindMeDateTime) {
            this.remindMeDateTime = remindMeDateTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    public class Vjvjchhvvuch246202113152462021 {

        @SerializedName("desc")
        @Expose
        private String desc;
        @SerializedName("dueDate")
        @Expose
        private String dueDate;
        @SerializedName("remindMeDateTime")
        @Expose
        private String remindMeDateTime;
        @SerializedName("title")
        @Expose
        private String title;

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getDueDate() {
            return dueDate;
        }

        public void setDueDate(String dueDate) {
            this.dueDate = dueDate;
        }

        public String getRemindMeDateTime() {
            return remindMeDateTime;
        }

        public void setRemindMeDateTime(String remindMeDateTime) {
            this.remindMeDateTime = remindMeDateTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

}
