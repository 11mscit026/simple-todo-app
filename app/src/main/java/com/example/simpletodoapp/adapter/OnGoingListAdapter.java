package com.example.simpletodoapp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.model.TaskModel;

import java.util.ArrayList;


public class OnGoingListAdapter extends RecyclerView.Adapter<OnGoingListAdapter.ViewHolder> {

    Activity activity;
    ArrayList<TaskModel> onGoingTaskModelArrayList = new ArrayList<>();

    boolean isCompleted = false;

    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onButtonDelete(int position, TaskModel allInOneObjectModel);

        void onButtonEdit(int position, TaskModel allInOneObjectModel);

        void onButtonComplete(int position, TaskModel allInOneObjectModel);

        void onButtonMoveToOnGoing(int position, TaskModel allInOneObjectModel);
    }

    public void setClickInterface(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    // RecyclerView recyclerView;
    public OnGoingListAdapter(Activity activity,
                              ArrayList<TaskModel> onGoingTaskModelArrayList, boolean isCompleted) {
        this.activity = activity;
        this.onGoingTaskModelArrayList = onGoingTaskModelArrayList;
        this.isCompleted = isCompleted;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_ongoind_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        TaskModel allInOneObjectModel = onGoingTaskModelArrayList.get(position);

        holder.tvTitle.setText(allInOneObjectModel.getTitle());
        holder.tvDesc.setText(allInOneObjectModel.getDesc());

        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {
                    onItemClickListener.onButtonDelete(position, allInOneObjectModel);
                }

            }
        });

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {
                    onItemClickListener.onButtonEdit(position, allInOneObjectModel);
                }

            }
        });

        holder.ivComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {
                    onItemClickListener.onButtonComplete(position, allInOneObjectModel);
                }

            }
        });
        holder.btnMoveToOnGoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {
                    onItemClickListener.onButtonMoveToOnGoing(position, allInOneObjectModel);
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return onGoingTaskModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llMain;
        public TextView tvTitle, tvDesc;
        ImageView ivRemove, ivEdit, ivComplete;
        Button btnMoveToOnGoing;

        public ViewHolder(View itemView) {
            super(itemView);
            this.llMain = itemView.findViewById(R.id.llMain);
            this.tvTitle = itemView.findViewById(R.id.tvTitle);
            this.tvDesc = itemView.findViewById(R.id.tvDesc);
            this.ivRemove = itemView.findViewById(R.id.ivRemove);
            this.ivEdit = itemView.findViewById(R.id.ivEdit);
            this.ivComplete = itemView.findViewById(R.id.ivComplete);
            this.btnMoveToOnGoing = itemView.findViewById(R.id.btnMoveToOnGoing);

            if (isCompleted) {
                this.ivRemove.setVisibility(View.GONE);
                this.ivEdit.setVisibility(View.GONE);
                this.ivComplete.setVisibility(View.GONE);
                this.btnMoveToOnGoing.setVisibility(View.VISIBLE);
            } else {
                this.ivRemove.setVisibility(View.VISIBLE);
                this.ivEdit.setVisibility(View.VISIBLE);
                this.ivComplete.setVisibility(View.VISIBLE);
                this.btnMoveToOnGoing.setVisibility(View.GONE);
            }

        }
    }
}  