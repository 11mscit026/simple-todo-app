package com.example.simpletodoapp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.model.SideMenuModel;

import java.util.ArrayList;

public class SideMenuAdapter extends BaseAdapter {

    private ArrayList<SideMenuModel> dataSet = new ArrayList<>();
    Activity activity;

    // View lookup cache
    private static class ViewHolder {
        TextView tvMenuTitle;
    }

    public SideMenuAdapter(ArrayList<SideMenuModel> data, Activity activity) {
        super();
//        super(activity, R.layout.row_side_menu, data);
        this.dataSet = data;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        SideMenuModel dataModel = dataSet.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.row_side_menu, parent, false);
            viewHolder.tvMenuTitle = convertView.findViewById(R.id.tvMenuTitle);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.tvMenuTitle.setText(dataModel.getMenuName());

        // Return the completed view to render on screen
        return convertView;
    }
}
