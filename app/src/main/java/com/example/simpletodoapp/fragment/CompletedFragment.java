package com.example.simpletodoapp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.activity.AddTaskFormActivity;
import com.example.simpletodoapp.activity.HomeActivity;
import com.example.simpletodoapp.adapter.OnGoingListAdapter;
import com.example.simpletodoapp.cmd.Constants;
import com.example.simpletodoapp.cmd.DebugLogger;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.model.TaskModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CompletedFragment extends Fragment implements OnGoingListAdapter.OnItemClickListener {


    View fragmentView = null;
    Activity activity = null;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference  databaseReference,databaseReferenceCompleted;

    ArrayList<TaskModel> onGoingTaskModelArrayList = new ArrayList<>();

    RecyclerView rvONGOING;

    OnGoingListAdapter onGoingListAdapter = null;

    public CompletedFragment() {
        // Required empty public constructor  
    }  
  
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_ongoing, container, false);

        activity = getActivity();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(SharedPreferencesComman.getStringName(SharedPreferencesComman.USER_ID))
                .child(Constants.onGoing);
        databaseReferenceCompleted = firebaseDatabase.getReference(SharedPreferencesComman.getStringName(SharedPreferencesComman.USER_ID))
                .child(Constants.Completed);

        initComponents();
        initClicks();

        return fragmentView;

    }

    private void initClicks() {

        fragmentView.findViewById(R.id.ivAddTask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, AddTaskFormActivity.class));
            }
        });

    }

    private void initComponents() {

        fragmentView.findViewById(R.id.ivAddTask).setVisibility(View.GONE);

        rvONGOING = fragmentView.findViewById(R.id.rvONGOING);
        rvONGOING.setLayoutManager(new LinearLayoutManager(activity,
                LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.ShowProgressDialog(activity, activity.getString(R.string.please_wait));
        databaseReferenceCompleted.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                onGoingTaskModelArrayList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    DebugLogger.e("26/06 Get getKey", postSnapshot.getKey());

                    TaskModel post = postSnapshot.getValue(TaskModel.class);
                    post.setMainKey(postSnapshot.getKey());
                    DebugLogger.e("26/06 Get Data", post.getDesc());

                    onGoingTaskModelArrayList.add(post);

                }

                HomeActivity.hideProgressDialog();
                if (onGoingTaskModelArrayList != null) {
                    onGoingListAdapter = new OnGoingListAdapter(activity, onGoingTaskModelArrayList,true);
                    rvONGOING.setAdapter(onGoingListAdapter);
                    onGoingListAdapter.setClickInterface(CompletedFragment.this);
                    onGoingListAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onButtonDelete(int position, TaskModel allInOneObjectModel) {

    }

    @Override
    public void onButtonEdit(int position, TaskModel allInOneObjectModel) {

    }

    @Override
    public void onButtonComplete(int position, TaskModel allInOneObjectModel) {

    }

    @Override
    public void onButtonMoveToOnGoing(int position, TaskModel allInOneObjectModel) {

        databaseReferenceCompleted.child(allInOneObjectModel.getMainKey()).removeValue();
        databaseReference.child(allInOneObjectModel.getMainKey()).setValue(allInOneObjectModel);

    }
}