package com.example.simpletodoapp.cmd;

import android.app.Activity;

import com.example.simpletodoapp.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Constants {

    public static String onGoing = "onGoing";
    public static String Completed = "Completed";

    public static ImageLoader imageLoader;
    public static DisplayImageOptions options;

    public static void initProfilePicImageLoader(Activity activity) {
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.logo)
                .showImageOnFail(R.drawable.logo)
                .showImageOnLoading(R.drawable.logo).build();
    }


}
