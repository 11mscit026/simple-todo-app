package com.example.simpletodoapp.cmd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;


import com.example.simpletodoapp.BuildConfig;
import com.example.simpletodoapp.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


public class commanUtils {

    public final static boolean isValidEmailAddress(String value) {

        if (commanUtils.isValidationEmpty(value)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches();
        }
    }


    public static void showKeyBoard(View v, Activity mActivity) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, 0);
    }

    public static void hideKeyBoard(View view, Activity mActivity) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static boolean checkInternetAwailable(final Context context, boolean canShowErrorDialogOnFail) {
        boolean isNetAvailable = false;

        if (context != null) {
            final ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (mConnectivityManager != null) {
                boolean mobileNetwork = false;
                boolean wifiNetwork = false;
                boolean mobileNetworkConnecetd = false;
                boolean wifiNetworkConnecetd = false;

                final NetworkInfo mobileInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                final NetworkInfo wifiInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mobileInfo != null) {
                    mobileNetwork = mobileInfo.isAvailable();
                }

                if (wifiInfo != null) {
                    wifiNetwork = wifiInfo.isAvailable();
                }

                if (wifiNetwork || mobileNetwork) {
                    if (mobileInfo != null)
                        mobileNetworkConnecetd = mobileInfo
                                .isConnectedOrConnecting();
                    wifiNetworkConnecetd = wifiInfo.isConnectedOrConnecting();
                }

                isNetAvailable = (mobileNetworkConnecetd || wifiNetworkConnecetd);
            }
            context.setTheme(R.style.AppTheme);
            if (!isNetAvailable && canShowErrorDialogOnFail) {
                Log.v("TAG", "context : " + context.toString());
                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            errorShowAlertWithFinish((Activity) context,
                                    context.getString(R.string.alert_check_internet_connection), false);
                        }
                    });
                }
            }
        }
        return isNetAvailable;
    }

    public static Boolean isValidationEmpty(String value) {

        if (value == null || value.isEmpty()
                || value.equalsIgnoreCase("null")
                || value.equalsIgnoreCase("") || value.length() == 0) {
            return true;
        } else {
            return false;
        }

    }

    public static Boolean isValidationEmptyWithZero(String value) {

        if (value == null || value.isEmpty() || value.equals("null")
                || value.length() <= 0 || value.equalsIgnoreCase("0")) {

            return true;
        } else {


            return false;
        }

    }

    public static boolean isValidPassword(String value) {

        if (TextUtils.isEmpty(value) || value.length() < 6) {
            return true;
        }

        return false;


    }

    public static AlertOkEventListener alertOkEventListener;

    public static interface AlertOkEventListener {
        void onOkClick();
    }

    public static void setAlertOkEventListener(AlertOkEventListener alertOkEventListener) {
        commanUtils.alertOkEventListener = alertOkEventListener;
    }

    public static void setCustomOkDialogClick(Activity activity, String message) {


        commanUtils.alertOkEventListener = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.app_name));

        //Setting message manually and performing action on button click
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();

                       /* if (commanUtils.alertOkEventListener != null) {
                            commanUtils.alertOkEventListener.onOkClick();
                        }*/


                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle(activity.getString(R.string.app_name));
        alert.show();

    }


    public static void errorShowAlertWithFinish(final Activity activity, String message, final boolean isFinish) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.ErrorAlertDialogStyle);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isFinish) {

                    if (!activity.isFinishing()) {
                        dialog.dismiss();
                        activity.finish();
                    }

                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public static void errorShowAlert(final Activity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.ErrorAlertDialogStyle);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builder.show();
    }

    @SuppressLint("PackageManagerGetSignatures") // test purpose
    public static void getSha1(Activity activity, String key) {
        try {
            final PackageInfo info = activity.getPackageManager()
                    .getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance(key);
                md.update(signature.toByteArray());

                final byte[] digest = md.digest();
                final StringBuilder toRet = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    if (i != 0) toRet.append(":");
                    int b = digest[i] & 0xff;
                    String hex = Integer.toHexString(b);
                    if (hex.length() == 1) toRet.append("0");
                    toRet.append(hex);
                }

                DebugLogger.e("SHA-1", key + " " + toRet.toString());
            }
        } catch (PackageManager.NameNotFoundException e1) {
            DebugLogger.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            DebugLogger.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            DebugLogger.e("exception", e.toString());
        }
    }


    public static AlertYesNoCancelEventListener alertYesNoCancelEventListener;


    public static void setAlertYesNoCancelEventListener(AlertYesNoCancelEventListener alertYesNoCancelEventListener) {
        commanUtils.alertYesNoCancelEventListener = alertYesNoCancelEventListener;
    }

    public static interface AlertYesNoCancelEventListener {
        void onYesClick();

        void onNoClick();

        void onCancelClick();

    }

    public static void setAlertYesNoCancelWithCustomListenerDialogDynamicName(Activity activity, String message,
                                                                              String str_yes,
                                                                              String str_no, String str_cancel,
                                                                              Boolean isCenterText) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
//        builder.setTitle(activity.getString(R.string.app_name));

        if (isCenterText) {

            //Title
            TextView title = new TextView(activity);
// You Can Customise your Title here
            title.setText(activity.getString(R.string.app_name));
            title.setGravity(Gravity.CENTER);
            title.setTextColor(Color.BLACK);
            title.setPadding(activity.getResources().getDimensionPixelSize(R.dimen._10sdp),
                    activity.getResources().getDimensionPixelSize(R.dimen._10sdp),
                    activity.getResources().getDimensionPixelSize(R.dimen._10sdp),
                    activity.getResources().getDimensionPixelSize(R.dimen._10sdp));
            title.setTypeface(title.getTypeface(), Typeface.BOLD);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen._15ssp));
            builder.setCustomTitle(title);
            //Title

            TextView myMsg = new TextView(activity);
            myMsg.setTextColor(activity.getResources().getColor(R.color.black));
//            myMsg.setTextSize(R.dimen._14ssp);
            myMsg.setText(message);
//            myMsg.setTextSize(14);
            myMsg.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen._12ssp));
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            builder.setView(myMsg);

        } else {
            builder.setTitle(activity.getString(R.string.app_name));
            builder.setMessage(message);
        }

        //Setting message manually and performing action on button click
        builder/*.setMessage(message)*/
                .setCancelable(false)
                .setPositiveButton(str_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //set positive button as a no
                        dialog.dismiss();
                        if (commanUtils.alertYesNoCancelEventListener != null) {
                            commanUtils.alertYesNoCancelEventListener.onNoClick();
                        }
                    }
                })
                .setNegativeButton(str_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        //set negative button as a yes
                        dialog.dismiss();
                        if (commanUtils.alertYesNoCancelEventListener != null) {
                            commanUtils.alertYesNoCancelEventListener.onYesClick();
                        }
                    }
                })
                .setNeutralButton(str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        //set negative button as a yes
                        dialog.dismiss();
                        if (commanUtils.alertYesNoCancelEventListener != null) {
                            commanUtils.alertYesNoCancelEventListener.onCancelClick();
                        }
                    }
                });
        //Creating dialog box
        android.app.AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle(activity.getString(R.string.app_name));
        alert.show();

    }


}
