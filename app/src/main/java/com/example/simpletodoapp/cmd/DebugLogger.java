package com.example.simpletodoapp.cmd;

import android.util.Log;

import com.example.simpletodoapp.BuildConfig;

public class DebugLogger {

    public static final boolean DEBUGGING = true;

    public static void e(final String tag, final Object message) {
        if (BuildConfig.DEBUG && DEBUGGING) {
            Log.e(tag, "" + message.toString());
        }
    }

}