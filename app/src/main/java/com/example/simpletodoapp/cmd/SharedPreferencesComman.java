package com.example.simpletodoapp.cmd;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.simpletodoapp.CommanApplicationClass;
import com.example.simpletodoapp.R;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;


public class SharedPreferencesComman {

    public static final String USER_ID = "USER_ID";
    public static final String UserObject = "UserObject";


    private static SharedPreferences get() {
        return CommanApplicationClass.getAppContext()
                .getSharedPreferences(CommanApplicationClass.getAppContext().getString(R.string.app_name),
                        Context.MODE_PRIVATE);
    }

    public static void clearPrefrence() {
        SharedPreferences preferences = CommanApplicationClass.getAppContext().
                getSharedPreferences(CommanApplicationClass.getAppContext().getString(R.string.app_name),
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static String getStringName(String Key) {
        return get().getString(Key, "");
    }

    public static void setStringName(String Key, String Value) {
        get().edit().putString(Key, Value).commit();
    }

    public static boolean getBoolenValue(String Key) {
        return get().getBoolean(String.valueOf(Key), false);
    }

    public static void setBoolenValue(String Key, boolean TrueorFalse) {
        get().edit().putBoolean(String.valueOf(Key), TrueorFalse).commit();
    }

    public static int getInteger(String Key) {
        return get().getInt(String.valueOf(Key), 0);
    }

    public static void setInteger(String Key, int value) {
        get().edit().putInt(String.valueOf(Key), value).commit();
    }

    public static void saveUserObject(String Key, FirebaseUser user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        DebugLogger.e("27/03 json saveUserObject -====> ", json);
        get().edit().putString(UserObject, json).commit();
    }


    public static FirebaseUser getUserObject(String Key) {
        Gson gson = new Gson();
        String json = SharedPreferencesComman.getStringName(Key);
        DebugLogger.e("27/03 json getUserObject -====> ", json);

        FirebaseUser obj = gson.fromJson(json, FirebaseUser.class);
        return obj;
    }

}

