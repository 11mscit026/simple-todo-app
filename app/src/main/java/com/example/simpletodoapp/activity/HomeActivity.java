package com.example.simpletodoapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.adapter.MyAdapter;
import com.example.simpletodoapp.adapter.SideMenuAdapter;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.cmd.commanUtils;
import com.example.simpletodoapp.model.SideMenuModel;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import okhttp3.internal.Util;

public class HomeActivity extends BaseActivity {


    Activity activity = null;

    DrawerLayout drawer_layout;

    ListView lvSideMenu;

    ArrayList<SideMenuModel> sideMenuModelArrayList = new ArrayList<>();
    SideMenuAdapter sideMenuAdapter = null;

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        activity = this;
        setSideMenu();
        initToolBar();
        initComponents();
        initClicks();

    }

    private void setSideMenu() {

        lvSideMenu = findViewById(R.id.lvSideMenu);

     /*   User can see the complete user details
○User can edit his/her details but only Full Name and Profile Picture
*/
        sideMenuModelArrayList.clear();
        sideMenuModelArrayList.add(new SideMenuModel(R.drawable.ic_user_gray,
                getString(R.string.home)));
        sideMenuModelArrayList.add(new SideMenuModel(R.drawable.ic_user_gray,
                getString(R.string.my_profile)));
        sideMenuModelArrayList.add(new SideMenuModel(R.drawable.ic_user_gray,
                getString(R.string.logout)));

        sideMenuAdapter = new SideMenuAdapter(sideMenuModelArrayList, activity);
        lvSideMenu.setAdapter(sideMenuAdapter);

        lvSideMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                drawer_layout.closeDrawer(GravityCompat.START);
                if (position == 0) {
                } else if (position == 2) {
                    FirebaseAuth.getInstance().signOut();
                    SharedPreferencesComman.clearPrefrence();
                    startActivity(new Intent(activity, LoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                }

            }
        });

    }

    private void initClicks() {


    }


    private void initToolBar() {

        drawer_layout = findViewById(R.id.drawer_layout);
//        drawer_layout.setDrawerListener(mDrawerToggle);

        findViewById(R.id.ivBack).setVisibility(View.GONE);
        findViewById(R.id.ivMenu).setVisibility(View.VISIBLE);
        findViewById(R.id.tvHeader).setVisibility(View.VISIBLE);

        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(activity.getString(R.string.app_name));

        findViewById(R.id.ivMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.openDrawer(GravityCompat.START);
            }
        });

    }


    private void initComponents() {
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        tabLayout.addTab(tabLayout.newTab().setText("OnGoing"));
        tabLayout.addTab(tabLayout.newTab().setText("Completed"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}