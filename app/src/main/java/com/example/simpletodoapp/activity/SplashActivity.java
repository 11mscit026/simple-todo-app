package com.example.simpletodoapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.cmd.commanUtils;

import okhttp3.internal.Util;

public class SplashActivity extends Activity {
    private Activity activity;
    private static int SPLASH_TIME_OUT = 725;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        setContentView(R.layout.activity_splash);
        activity = SplashActivity.this;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!commanUtils.isValidationEmpty(SharedPreferencesComman.getStringName(SharedPreferencesComman.USER_ID))
                        || SharedPreferencesComman.getUserObject(SharedPreferencesComman.UserObject) != null) {
                    startActivity(new Intent(activity, HomeActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();

                } else {

                    startActivity(new Intent(activity, LoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();

                }

            }
        }, SPLASH_TIME_OUT);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
