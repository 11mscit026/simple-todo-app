package com.example.simpletodoapp.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.cmd.Constants;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.cmd.commanUtils;
import com.example.simpletodoapp.model.TaskModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class AddTaskFormActivity extends BaseActivity {

    Activity activity = null;
    EditText etTitle, etDesc;
    TextView tvRemindMe, tvDueDate;

    FirebaseDatabase firebaseDatabase;

    // creating a variable for our Database
    // Reference for Firebase.
    DatabaseReference databaseReference;

    TaskModel editallInOneObjectModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task_form);
        activity = this;

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(SharedPreferencesComman.getStringName(SharedPreferencesComman.USER_ID))
                .child(Constants.onGoing);

        getIntentData();
        initToolBar();
        initComponents();
        initClicks();
        setOldDetails();

    }

    private void setOldDetails() {

        if (editallInOneObjectModel != null) {
            etTitle.setText(editallInOneObjectModel.getTitle());
            etDesc.setText(editallInOneObjectModel.getDesc());
            tvRemindMe.setText(editallInOneObjectModel.getRemindMeDateTime());
            tvDueDate.setText(editallInOneObjectModel.getDueDate());
        }

    }

    private void getIntentData() {

        editallInOneObjectModel = (TaskModel) getIntent().getSerializableExtra("allInOneObjectModel");

    }

    private void initClicks() {

        findViewById(R.id.btnAddTask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidation()) {

                    TaskModel taskModel = new TaskModel(
                            etTitle.getText().toString().trim(),
                            etDesc.getText().toString().trim(),
                            tvRemindMe.getText().toString().trim(),
                            tvDueDate.getText().toString().trim()
                    );

                    if (commanUtils.checkInternetAwailable(activity, true)) {

                       /* databaseReference.child(etTitle.getText().toString().trim() + "_" +
                                etDesc.getText().toString().trim() + "_" +
                                tvDueDate.getText().toString().trim()).setValue(taskModel);*/
//                        onBackPressed();

                        if (editallInOneObjectModel != null) {
                            databaseReference.child(editallInOneObjectModel.getMainKey()).setValue(taskModel);
                        } else {
                            databaseReference.push().setValue(taskModel);
                        }

                        onBackPressed();

/*

                        databaseReference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                // inside the method of on Data change we are setting
                                // our object class to our database reference.
                                // data base reference will sends data to firebase.
                                databaseReference.push().setValue(taskModel);

                                onBackPressed();

                                // after adding this data we are showing toast message.
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                // if the data is not added or it is cancelled then
                                // we are displaying a failure toast message.
                                Toast.makeText(activity, "Fail to add data " + error, Toast.LENGTH_SHORT).show();
                            }
                        });
*/


                    }

                }


            }
        });

        findViewById(R.id.tvRemindMe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDatePicker();
            }
        });

        findViewById(R.id.tvDueDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDueDatePicker();
            }
        });


    }

    private int mYear, mMonth, mDay, mHour, mMinute;

    public void selectDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

//                        tvRemindMe.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        selectTimePicker(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void selectDueDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        tvDueDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void selectTimePicker(String date) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        tvRemindMe.setText(date + " " + hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }

    private void initToolBar() {

        findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
        findViewById(R.id.tvHeader).setVisibility(View.VISIBLE);

        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(activity.getString(R.string.add_task));

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    private void initComponents() {

        etTitle = findViewById(R.id.etTitle);
        etDesc = findViewById(R.id.etDesc);
        tvRemindMe = findViewById(R.id.tvRemindMe);
        tvDueDate = findViewById(R.id.tvDueDate);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isValidation() {

        if (commanUtils.isValidationEmpty(etTitle.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_title));
            return false;
        } else if (commanUtils.isValidationEmpty(etDesc.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_desc));
            return false;
        } else if (commanUtils.isValidationEmpty(tvRemindMe.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_select_remind_me));
            return false;
        } else if (commanUtils.isValidationEmpty(tvDueDate.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_select_due_date));
            return false;
        }

        return true;
    }

}
