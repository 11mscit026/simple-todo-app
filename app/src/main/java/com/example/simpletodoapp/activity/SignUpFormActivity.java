package com.example.simpletodoapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.cmd.commanUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import id.zelory.compressor.Compressor;

public class SignUpFormActivity extends BaseActivity {

    Activity activity = null;
    EditText etFullName, etEmailAddress, etPassword;
    private FirebaseAuth auth;

    ImageView ivProfile;

    private final int PICK_IMAGE_GALLERY = 2;
    //    private Camera camera;
    private File imgFile;
    Uri outputUri = null;
    Uri inputUri = null;

    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_form);
        activity = this;
        auth = FirebaseAuth.getInstance();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        initToolBar();
        initComponents();
        initClicks();

    }

    private void initClicks() {

        findViewById(R.id.btnCreateAnAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidation()) {

                    if (commanUtils.checkInternetAwailable(activity, true)) {

                        ShowProgressDialog(activity, activity.getString(R.string.please_wait));

//                        uplodImage();
                        SignUpDetailsSaveDB(null);

                    }

                }


            }
        });


        findViewById(R.id.rlProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                checkStoragePermission();

            }
        });

    }

    private void uplodImage() {

        StorageReference ref
                = storageReference
                .child(
                        "images/"
                                + UUID.randomUUID().toString());

        // adding listeners on upload
        // or failure of image
        ref.putFile(Uri.fromFile(imgFile))
                .addOnSuccessListener(
                        new OnSuccessListener<UploadTask.TaskSnapshot>() {

                            @Override
                            public void onSuccess(
                                    UploadTask.TaskSnapshot taskSnapshot) {

                                Uri downloadUrl = taskSnapshot.getStorage().getDownloadUrl().getResult();
                                SignUpDetailsSaveDB(downloadUrl);
                                // Image uploaded successfully
                                // Dismiss dialog

                            }
                        })

                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        commanUtils.errorShowAlert(activity, activity.getString(R.string.authentication_failed)
                                + "\n\n" + activity.getString(R.string.some_technical_problem_occur));


                    }
                })
                .addOnProgressListener(
                        new OnProgressListener<UploadTask.TaskSnapshot>() {

                            // Progress Listener for loading
                            // percentage on the dialog box
                            @Override
                            public void onProgress(
                                    UploadTask.TaskSnapshot taskSnapshot) {
                            }
                        });


    }

    private void checkStoragePermission() {

        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now

                            commanUtils.setAlertYesNoCancelWithCustomListenerDialogDynamicName(activity,
                                    activity.getString(R.string.please_select_options),
                                    activity.getString(R.string.gallery),
                                    activity.getString(R.string.camera),
                                    activity.getString(R.string.cancel), false);

                            commanUtils.setAlertYesNoCancelEventListener(new commanUtils.AlertYesNoCancelEventListener() {
                                @Override
                                public void onYesClick() {
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                                }

                                @Override
                                public void onNoClick() {
                                    /*camera = new Camera.Builder()
                                            .resetToCorrectOrientation(true)// it will rotate the camera bitmap to the correct orientation from meta data
                                            .setTakePhotoRequestCode(1)
                                            .setImageFormat(Camera.IMAGE_JPEG)
                                            .build(activity);
                                    try {
                                        camera.takePicture();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }*/

                                }

                                @Override
                                public void onCancelClick() {

                                }
                            });

                        } else {
                            commanUtils.errorShowAlert(activity,
                                    activity.getString(R.string.alert_storage_permission));
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            commanUtils.errorShowAlert(activity,
                                    activity.getString(R.string.alert_storage_permission));
                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();

                    }


                })
                .onSameThread()
                .check();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == activity.RESULT_OK) {

            /*if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
                Bitmap bitmap = camera.getCameraBitmap();
                if (bitmap != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getString(R.string.app_name) + "/Camera";
                    File dir = new File(file_path);
                    if (!dir.exists())
                        dir.mkdirs();
                    String format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(new Date());
                    File file = new File(dir, format + ".png");
                    FileOutputStream fo;
                    try {
                        fo = new FileOutputStream(file);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imgFile = file;
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(activity).compressToFile(imgFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imgFile = compressedImageFile;
                    inputUri = Uri.fromFile(new File(String.valueOf(imgFile)));
                    outputUri = Uri.fromFile(new File(getCacheDir(), "cropped1"));
                    Crop.of(inputUri, outputUri).asSquare().start(activity);
                } else {
//                    Toast.makeText(activity, activity.getString(R.string.alert_picture_not_taken), Toast.LENGTH_SHORT).show();

                    commanUtils.errorShowAlert(activity,
                            activity.getString(R.string.alert_picture_not_taken));

                }
            } else*/
            if (requestCode == PICK_IMAGE_GALLERY) {

                Uri selectedImage = data.getData();

                try {

                    imgFile = FileUtils.getFile(activity, selectedImage);

                    File compressedImageFile = new Compressor(this).compressToFile(imgFile);
                    imgFile = compressedImageFile;
                    inputUri = Uri.fromFile(new File(String.valueOf(imgFile)));
                    outputUri = Uri.fromFile(new File(getCacheDir(), "cropped1"));
                    Crop.of(inputUri, outputUri).asSquare().start(activity);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == Crop.REQUEST_CROP) {

                try {

                    imgFile = new File(FileUtils.getPath(activity, Crop.getOutput(data)));
                    Bitmap compressedImageBitmap = new Compressor(activity).compressToBitmap(imgFile);

                    ivProfile.setImageBitmap(compressedImageBitmap);

                    if (imgFile == null || !imgFile.exists()) {
                        commanUtils.errorShowAlert(activity,
                                activity.getString(R.string.alert_picture_not_taken));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    commanUtils.errorShowAlert(activity,
                            activity.getString(R.string.alert_picture_not_taken));
                }

            }

        }


    }

    private void initToolBar() {

        findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
        findViewById(R.id.tvHeader).setVisibility(View.VISIBLE);

        TextView tvHeader = findViewById(R.id.tvHeader);
        tvHeader.setText(activity.getString(R.string.sign_up));

        findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    private void initComponents() {

        ivProfile = findViewById(R.id.ivProfile);

        etFullName = findViewById(R.id.etFullName);
        etEmailAddress = findViewById(R.id.etEmailAddress);
        etPassword = findViewById(R.id.etPassword);

    }

    private void SignUpDetailsSaveDB(Uri downloadUrl) {

        auth.createUserWithEmailAndPassword(etEmailAddress.getText().toString().trim(),
                etPassword.getText().toString().trim())
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        Toast.makeText(SignupActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                        hideProgressDialog();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            /*Toast.makeText(SignupActivity.this, "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();*/

                            commanUtils.errorShowAlert(activity, activity.getString(R.string.authentication_failed)
                                    + "\n\n" + task.getException().getMessage());

                        } else {

                            FirebaseUser firebaseUser = task.getResult().getUser();
                            if (firebaseUser != null) {
                                SharedPreferencesComman.setStringName(SharedPreferencesComman.USER_ID,
                                        firebaseUser.getUid());

                                SharedPreferencesComman.saveUserObject(SharedPreferencesComman.UserObject,
                                        firebaseUser);

                                startActivity(new Intent(activity, HomeActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            } else {
                                commanUtils.errorShowAlert(activity, activity.getString(R.string.authentication_failed)
                                        + "\n\n" + activity.getString(R.string.some_technical_problem_occur));

                            }

                        }
                    }

                });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isValidation() {

        if (imgFile == null || !imgFile.exists()) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_select_image));
            return false;
        } else if (commanUtils.isValidationEmpty(etFullName.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_full_name));
            return false;
        } else if (commanUtils.isValidationEmpty(etEmailAddress.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_email_address));
            return false;
        } else if (!commanUtils.isValidEmailAddress(etEmailAddress.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_valid_email_address));
            return false;
        } else if (commanUtils.isValidationEmpty(etPassword.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_password));
            return false;
        } else if (commanUtils.isValidPassword(etPassword.getText().toString().trim())) {
            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_password_six_character));
            return false;
        }

        return true;
    }

}
