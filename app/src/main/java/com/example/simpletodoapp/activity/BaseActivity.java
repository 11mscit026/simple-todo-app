package com.example.simpletodoapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;

import androidx.appcompat.app.AppCompatActivity;

import com.example.simpletodoapp.R;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDoalog;


    /*public void ShowProgressDialog(Activity activity) {

        if (progressDoalog != null && progressDoalog.isShowing()) {
            progressDoalog = null;
            progressDoalog = new ProgressDialog(activity);
            progressDoalog.setMessage(getString(R.string.please_wait)); // Setting Message
            progressDoalog.setTitle(activity.getString(R.string.app_name)); // Setting Title
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDoalog.show(); // Display Progress Dialog
            progressDoalog.setCancelable(false);
        } else {
            progressDoalog = null;
            progressDoalog = new ProgressDialog(activity);
            progressDoalog.setMessage(getString(R.string.please_wait)); // Setting Message
            progressDoalog.setTitle(activity.getString(R.string.app_name)); // Setting Title
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDoalog.show(); // Display Progress Dialog
            progressDoalog.setCancelable(false);
        }
    }

    public void HideProgressDialog(Activity activity) {

        if (progressDoalog != null && progressDoalog.isShowing()) {
            progressDoalog.dismiss();
            progressDoalog = null;
        }

    }*/

    public static ProgressDialog progressDialog;

    public static void ShowProgressDialog(Activity activity, String message) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        try {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
