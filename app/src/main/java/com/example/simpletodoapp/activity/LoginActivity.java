package com.example.simpletodoapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.example.simpletodoapp.R;
import com.example.simpletodoapp.cmd.SharedPreferencesComman;
import com.example.simpletodoapp.cmd.commanUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class LoginActivity extends BaseActivity {

    Activity activity = null;
    EditText etEmailAddress, etPassword;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activity = this;
        auth = FirebaseAuth.getInstance();


        initToolBar();
        initComponents();
        initClicks();

        commanUtils.getSha1(activity, "SHA");


    }

    private void initClicks() {

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidation()) {

                    ShowProgressDialog(activity, activity.getString(R.string.please_wait));
                    LoginCall();

                }

            }
        });

        findViewById(R.id.llSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity, SignUpFormActivity.class));

            }
        });

    }

    private void LoginCall() {


        auth.signInWithEmailAndPassword(etEmailAddress.getText().toString().trim(),
                etPassword.getText().toString().trim())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        hideProgressDialog();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            /*Toast.makeText(SignupActivity.this, "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();*/

                            commanUtils.errorShowAlert(activity, activity.getString(R.string.authentication_failed)
                                    + "\n\n" + task.getException().getMessage());

                        } else {

                            FirebaseUser firebaseUser = task.getResult().getUser();
                            if (firebaseUser != null) {
                                SharedPreferencesComman.setStringName(SharedPreferencesComman.USER_ID,
                                        firebaseUser.getUid());

                                SharedPreferencesComman.saveUserObject(SharedPreferencesComman.UserObject,
                                        firebaseUser);

                                startActivity(new Intent(activity, HomeActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            } else {
                                commanUtils.errorShowAlert(activity, activity.getString(R.string.authentication_failed)
                                        + "\n\n" + activity.getString(R.string.some_technical_problem_occur));

                            }

                        }

                    }
                });

    }


    private void initToolBar() {


    }


    private void initComponents() {

        etEmailAddress = findViewById(R.id.etEmailAddress);
        etPassword = findViewById(R.id.etPassword);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean isValidation() {

        if (commanUtils.isValidationEmpty(etEmailAddress.getText().toString().trim())) {

            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_email_address));
            return false;

        } else if (commanUtils.isValidationEmpty(etPassword.getText().toString().trim())) {

            commanUtils.setCustomOkDialogClick(activity, getString(R.string.alert_enter_password));
            return false;

        }

        return true;
    }


}
